# From the Front splash page

A simple HTML/CSS page to promote From the front events.

### Requirements:
* nodejs and npm (refer to package.json)
* sass

### Install dependencies:
```sh
npm i -g gulp bower
npm i
bower i
```

The source files are in src folder.
Run:

```sh
gulp build
```

to build files and run - watched - server.

Run:

```sh
gulp clean
```
to remove build_dev and build_prod folders.


### TODO:
* TODOs in the code
* Style heading for mobile, not 100% height (?)
* General styling for big screens
* Clean class naming, better BEM
* Crossbrowser
