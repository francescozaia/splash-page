(function(){

  'use strict';

  var gulp = require('gulp'),
    gulp_connect = require('gulp-connect'),
    gulp_jshint = require('gulp-jshint'),
    gulp_uglify = require('gulp-uglify'),
    gulp_concat = require('gulp-concat'),
    gulp_rename = require('gulp-rename'),
    gulp_sass = require('gulp-sass'),
    gulp_minifyHTML = require('gulp-minify-html'),
    gulp_minifyCSS = require('gulp-minify-css'),
    jshint_stylish = require('jshint-stylish'),
    map = require('map-stream'),
    del = require('del');

  /**
   *  _base_path source folder
   */
  var _base_path = './src';

  /**
   *  _build_dev development build folder
   *  _build_prod production build folder
   */
  var _build_dev = './build_dev';
  var _build_prod = './build_prod';

  var js_source = _base_path + '/js/**/*.js';
  var sass_source = _base_path + '/sass/**/*.scss';
  var html_source = _base_path + '/**/*.html';
  var files_source = _base_path + '/**/*.{jpg,png,gif,svg}';

  gulp.task('webserver', function() {
    gulp_connect.server({
      root: 'build_dev',
      /* root: 'build_prod', */
      livereload: true
    });
  });

  /**
   *  clean
   *    removes folders
   */
  gulp.task('clean', function (cb) {
    del([
      _build_prod,
      _build_dev
    ], cb);
  });

  /**
   *  lint, jscopy, jsminify
   *    DEV: files linting, copy js to _build_dev
   *    PROD: files linting, concatenate, uglify, copy to _build_prod
   */
  gulp.task('lint',  function() {

    return gulp.src( js_source )
      .pipe( gulp_jshint( '.jshintrc' ) )
      .pipe( gulp_jshint.reporter( jshint_stylish ) )
      .pipe( map( function ( file, callback ) {
          callback( !file.jshint.success, file );
      }));

  });

  gulp.task('js-copy', [], function() {

    return gulp.src(js_source, { base: _base_path + '/js' })
      .pipe(gulp.dest( _build_dev + '/js'));

  });

  gulp.task('js-minify', [], function() {

    return gulp.src( js_source, { base: _base_path } )
      .pipe(gulp_concat('concat.js'))
      .pipe(gulp_uglify())
      .pipe(gulp_rename({ extname: '.min.js' }))
      .pipe(gulp.dest( _build_prod + '/js' ));

  });

  /**
   *  sass
   *    DEV: convert to css, copy to _build_dev
   *    PROD: convert to css, minify, copy to _build_prod
   */
  gulp.task('sass', function () {

    return gulp.src( sass_source, { base: _base_path + '/sass' })
        .pipe(gulp_sass())
        .pipe(gulp.dest( _build_dev + '/css' ))
        .pipe(gulp_minifyCSS())
        .pipe(gulp.dest( _build_prod + '/css' ));

  });

  /**
   *  html-copy, files-copy
   *    DEV: copy to _build_dev
   *    PROD: minify html, copy to _build_prod (TODO use different src for JS files in prod)
   */
  gulp.task('html-copy', function () {

    return gulp.src( [
      html_source,

      ], { base: _base_path })
      .pipe(gulp.dest( _build_dev ))
      .pipe(gulp_minifyHTML())
      .pipe(gulp.dest( _build_prod ));

  });

  gulp.task('files-copy', function () {

    return gulp.src( files_source, { base: _base_path })
      .pipe(gulp.dest( _build_dev ))
      .pipe(gulp.dest( _build_prod ));

    });

  gulp.task('watch', function(){

    var sourcefiles = [
      '.jshintrc',
      'gulpfile.js',
      js_source,
      sass_source,
      html_source
    ];
    gulp.watch( sourcefiles, ['files-copy', 'html-copy', 'js-copy', 'js-minify', 'sass'] );
  });


  gulp.task('build', ['files-copy', 'html-copy', 'js-copy', 'js-minify', 'sass', 'webserver', 'watch']);

})();
